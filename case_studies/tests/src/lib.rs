#![feature(register_tool)]
#![register_tool(rr)]
#![feature(custom_inner_attributes)]
#![feature(stmt_expr_attributes)]
#![allow(dead_code)]

mod enums;
mod structs;
mod char;
mod traits;
mod statics;

mod vec_client;
mod mixed;
mod closures;
mod references;
mod option;
